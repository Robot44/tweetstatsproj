﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using NuGet.Protocol.Core.v3;
using TwitterStatProj.Models;

namespace TwitterStatProj.Controllers
{
    [Authorize]
    [Route("api/user/{userId}/[controller]")]
    public class TwitterController
    {
        public TwitterController(IUserRepository repository)
        {
           Repository = repository;
        }
        private IUserRepository Repository { get; set; }
        
        [Route("TagFrequency/{tag}")]
        [HttpGet]
        public string HashtagFrequency(string userId,char tag)
        {
            var posts = Repository.GetAllPosts(userId);
            var sb = new StringBuilder();
            foreach (var postDto in posts)
            {
                sb.Append(postDto.Text + "\n");
            }
            var str = sb.ToString();
            var wordFrequency = Regex.Matches(str, tag+@"\w+").Cast<Match>()
                .Select(s => new {word = s.Value, cnt = 1})
                .GroupBy(s => s.word, StringComparer.CurrentCultureIgnoreCase)
                .Select(s => new {s.Key, cnt = s.Count()})
                .Where(s => s.Key.Length > 3)
                .OrderByDescending(s=> s.cnt).Take(10);
            return wordFrequency.ToJson();
        }

        [Route("dep/{depType}")]
        [Route("dep/{depType}/{subject}")]
        [HttpGet]
        public string Amod(string userId, string depType,string subject)
        {
            var posts = Repository.GetAllPosts(userId);
            List<string> sentences = new List<string>();
            foreach (var postDto in posts)
            {
                //remove links
                var text = Regex.Replace(postDto.Text, @"(https ?://([-\w\.]+)+(:\d+)?(/([\w/_\.]*(\?\S+)?)?)?)", "");
                //remove quated text
                text = Regex.Replace(text, @"(""[^""]*"")" ,"");
                //split into sentences
                sentences.AddRange(Regex.Split(text, @"(?<=[\.!\?])\s+"));
            }
            sentences = sentences.Where(p => p.Length > 4).ToList();
            var wholeText = new StringBuilder();
            foreach (var sentence in sentences)
            {
                wholeText.Append(sentence);
            }
            var parameters = "?properties=\"annotators\":\"depparse\"";
            var uri = "http://localhost:9000/" + parameters;
            using (var client = new HttpClient())
            {
                HttpContent content = new ObjectContent<string>(wholeText.ToString(), new JsonMediaTypeFormatter());
                var response = client.PostAsync(uri, content).Result;
                if (response.IsSuccessStatusCode)
                {
                    var responseContent = response.Content;
                    string responseString = responseContent.ReadAsStringAsync().Result;
                    JObject jObject = JObject.Parse(responseString);
                    if (subject == null && depType != null)
                    return jObject["sentences"]
                        .SelectMany(i => i["basicDependencies"])
                        .Where(j => j["dep"].Value<string>() == depType).ToJson();
       
                    return jObject["sentences"]
                        .SelectMany(i => i["basicDependencies"])
                        .Where(j => j["dep"].Value<string>() == depType)
                        .Where(j => j["governorGloss"].Value<string>() == subject)
                        .Select(j => j["dependentGloss"].Value<string>()).ToJson();
                }
                return null;
            }
        }
    }
}
