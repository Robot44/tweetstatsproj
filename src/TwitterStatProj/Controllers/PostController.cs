﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TwitterStatProj.Models;

namespace TwitterStatProj.Controllers
{
    [Authorize]
    [Route("api/user/{userId}/[controller]")]
    public class PostController : Controller
    {
        public PostController(IUserRepository repository)
        {
            Repository = repository;
        }
        public IUserRepository Repository { get; set; }

        [HttpGet(Name = "GetPosts")]
        public IEnumerable<PostDTO> Get(string userId)
        {
            return Repository.GetAllPosts(userId);
        }

        [HttpGet("{id}", Name = "GetPost")]
        public IActionResult Get(int id, string userId)
        {
            var item = Repository.FindPost(id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult Post([FromBody]List<PostDTO> items, string userId)
        {
            if (items == null || Repository.FindUser(userId) == null)
            {
                return BadRequest();
            }
            foreach (var item in items)
            {
                var lastItem = Repository.AddPost(item, userId);
            }
            return new ObjectResult(items);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var post = Repository.FindPost(id);
            if (post == null)
            {
                return NotFound();
            }
            Repository.RemovePost(id);
            return new NoContentResult();
        }
    }
}
