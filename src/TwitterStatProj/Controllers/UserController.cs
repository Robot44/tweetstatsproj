﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using TwitterStatProj.Models;

namespace TwitterStatProj.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        public UserController(IUserRepository repository)
        {
            Repository = repository;
        }

        public IUserRepository Repository { get; set; }

        [HttpGet]
        public IEnumerable<UserDTO> GetAllUsers()
        {
            return Repository.GetAllUsers().ToList();
        }

        [HttpGet("{id}", Name = "GetUser")]
        public IActionResult GetById(string id)
        {
            var item = Repository.FindUser(id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult Create([FromBody] UserDTO item)
        {
            if (item?.Key == null || Repository.FindUser(item.Key) != null)
            {
                return BadRequest();
            }     
            Repository.AddUser(item);
            return CreatedAtRoute("GetUser", new { id = item.Key }, item);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var user = Repository.FindUser(id);
            if (user == null)
            {
                return NotFound();
            }

            Repository.RemoveUser(id);
            return new NoContentResult();
        }

        [HttpPut("{id}")]
        public IActionResult Update([FromBody] UserDTO item, string id)
        {
            if (item?.Key == null)
            {
                return BadRequest();
            }

            var user = Repository.FindUser(id);
            if (user == null)
            {
                return NotFound();
            }
            Repository.UpdateUser(item, id);
            return new NoContentResult();
        }
    }
}
