﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TwitterStatProj.Migrations
{
    public partial class RemovedUnnecessaryFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsComplete",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "ScreenName",
                table: "Users");

            migrationBuilder.AddColumn<DateTime>(
                name: "updatedAt",
                table: "Users",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "updatedAt",
                table: "Users");

            migrationBuilder.AddColumn<bool>(
                name: "IsComplete",
                table: "Users",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "ScreenName",
                table: "Users",
                nullable: true);
        }
    }
}
