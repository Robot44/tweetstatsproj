﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TwitterStatProj.Models
{
    public class Post
    {
        public Post() { }
        public int PostId { get; set; }
        public DateTime PostedAt { get; set; }
        public string Text { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
