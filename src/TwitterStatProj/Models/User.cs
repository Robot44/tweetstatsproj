﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace TwitterStatProj.Models
{
    public class UserContext : DbContext
    {
        public UserContext(DbContextOptions<UserContext> options) : base(options){}
        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }
    }
    public class User
    {
        public string Key { get; set; }
        public int UserId { get; set; }
        public virtual List<Post> Posts { get; set; }
        public DateTime updatedAt { get; set; }
    }
}
