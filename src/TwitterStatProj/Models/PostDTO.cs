﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TwitterStatProj.Models
{
    public class PostDTO
    {
        public string Text { get; set; }
        public DateTime PostedAt { get; set; }
        public string OwnerKey { get; set; }
        public int PostId { get; set; }
    }
}
