﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace TwitterStatProj.Models
{
    public class UserRepository: IUserRepository
    {
        private UserContext _context;
        public UserRepository(UserContext context)
        {
            _context = context;
        }

        public IEnumerable<UserDTO> GetAllUsers()
        {
            return _context.Users.Select(u => new UserDTO {Key = u.Key});
        }

        public void AddUser(UserDTO item)
        {
            _context.Users.Add(new User {Key = item.Key, updatedAt = DateTime.Now});
            _context.SaveChanges();
        }

        public UserDTO FindUser(string key)
        {
            var item = _context.Users.Where(u => u.Key == key).Select(u => new UserDTO { Key = u.Key }).FirstOrDefault();
            return item;
        }

        public UserDTO RemoveUser(string key)
        {
            var item = _context.Users.FirstOrDefault(x => x.Key == key);
            var dto = new UserDTO { Key = item.Key};
            _context.Remove(item);
            _context.SaveChanges();
            return dto;
        }

        public void UpdateUser(UserDTO item, string userKey)
        {
            var user = _context.Users.FirstOrDefault(x => x.Key == userKey);
            if (_context.Users.FirstOrDefault(p => p.Key == item.Key) != null || user == null) return;
            user.Key = item.Key;
            user.updatedAt = DateTime.Now;
            _context.SaveChanges();
        }

        public PostDTO AddPost(PostDTO item, string userId)
        {
            var user = _context.Users.FirstOrDefault(x => x.Key == userId);
            var post = new Post {PostedAt = item.PostedAt, Text = item.Text, User = user};
            if (user.Posts == null)
            {
                user.Posts = new List<Post>();
            }
            user.Posts.Add(post);
            _context.SaveChanges();
            return this.FindPost(post.PostId);
        }

        public IEnumerable<PostDTO> GetAllPosts(string userId)
        {
            return
                _context.Posts.Where(p => p.User.Key == userId)
                    .Select(p => new PostDTO {OwnerKey = p.User.Key, PostedAt = p.PostedAt, Text = p.Text, PostId = p.PostId })
                    .ToList();
        }

        public PostDTO FindPost(int id)
        {
            return
                _context.Posts.Where(p => p.PostId == id)
                    .Select(p => new PostDTO {Text = p.Text, PostedAt = p.PostedAt, OwnerKey = p.User.Key, PostId = p.PostId})
                    .FirstOrDefault();
        }

        public PostDTO RemovePost( int id)
        {
            var item = _context.Posts.FirstOrDefault(x => x.PostId == id);
            var dto = new PostDTO {OwnerKey = item.User.Key, PostedAt = item.PostedAt, Text = item.Text, PostId = item.PostId };
            _context.Remove(item);
            _context.SaveChanges();
            return dto;
        }

        public void UpdatePost(PostDTO item, int id)
        {
            var post = _context.Posts.FirstOrDefault(x => x.PostId == id);
            post.Text = item.Text;
            post.PostedAt = item.PostedAt;
            _context.SaveChanges();
        }
    }
}
