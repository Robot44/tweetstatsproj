﻿using System.Collections.Generic;

namespace TwitterStatProj.Models
{
    public interface IUserRepository
    {
        void AddUser(UserDTO item);
        IEnumerable<UserDTO> GetAllUsers();
        UserDTO FindUser(string key);
        UserDTO RemoveUser(string key);
        void UpdateUser(UserDTO item, string userKey);
        PostDTO AddPost(PostDTO item, string userId);
        IEnumerable<PostDTO> GetAllPosts(string userId);
        PostDTO FindPost(int id);
        PostDTO RemovePost(int id);
        void UpdatePost(PostDTO item, int id);
    }
}