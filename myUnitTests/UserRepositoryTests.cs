﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NuGet.Protocol.Core.v3;
using TwitterStatProj.Models;
using Xunit;

namespace myUnitTests
{
    public class UserRepositoryTests
    {
        private static DbContextOptions<UserContext> CreateNewContextOptions()
        {
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();
            var builder = new DbContextOptionsBuilder<UserContext>();
            builder.UseInMemoryDatabase()
                .UseInternalServiceProvider(serviceProvider);
            return builder.Options;
        }

        [Fact]
        public void Add_User_to_Database()
        {
            var options = CreateNewContextOptions();
            using (var context = new UserContext(options))
            {
                var service = new UserRepository(context);
                service.AddUser(new UserDTO {Key = "newUser"});
            }
            using (var context = new UserContext(options))
            {
                Assert.Equal(1, context.Users.Count());
                Assert.Equal(new UserDTO {Key = "newUser"}.Key, context.Users.Single().Key);
            }
        }

        [Fact]
        public void Add_Remove_User_From_Database()
        {
            var options = CreateNewContextOptions();
            using (var context = new UserContext(options))
            {
                var service = new UserRepository(context);
                service.AddUser(new UserDTO {Key = "newUser"});
                service.RemoveUser("newUser");
            }
            using (var context = new UserContext(options))
            {
                Assert.Equal(0, context.Users.Count());
            }
        }

        [Fact]
        public void Get_All_Users_From_Database()
        {
            var options = CreateNewContextOptions();
            using (var context = new UserContext(options))
            {
                var service = new UserRepository(context);
                service.AddUser(new UserDTO {Key = "newUser"});
                service.AddUser(new UserDTO {Key = "anotherUser"});
                var result = service.GetAllUsers();
                var users = new List<UserDTO> {new UserDTO {Key = "newUser"}, new UserDTO {Key = "anotherUser"}};
                Assert.Equal(users.ToJson(),result.ToJson());
            }
        }

        [Fact]
        public void Find_User_By_Id_In_Database()
        {
            var options = CreateNewContextOptions();
            using (var context = new UserContext(options))
            {
                var service = new UserRepository(context);
                service.AddUser(new UserDTO { Key = "newUser" });
                service.AddUser(new UserDTO { Key = "anotherUser" });
                var result = service.FindUser("newUser");
                var users = new UserDTO { Key = "newUser" };
                Assert.Equal(users.ToJson(), result.ToJson());
            }
        }

    }
}