﻿using System;
using System.Collections.Generic;
using Moq;
using TwitterStatProj.Controllers;
using TwitterStatProj.Models;
using Xunit;

namespace myUnitTests
{
    public class TwitterControllerTests
    {
        [Fact]
        public void Count_Correct_Number_Of_Tags()
        {
            var mockRepository = new Mock<IUserRepository>();
            var postList = new List<PostDTO>();
            postList.Add(new PostDTO { OwnerKey = "TestUser", PostId = 1, PostedAt = DateTime.Now, Text = "#UnitTest This is test string. #UnitTest #UnitTest" });
            mockRepository.Setup(rep => rep.GetAllPosts(It.IsAny<string>())).Returns(postList);
            var controller = new TwitterController(mockRepository.Object);
            var result = controller.HashtagFrequency("Username", '#');
            Assert.Equal("[{\"Key\":\"#UnitTest\",\"cnt\":3}]", result);
        }

        [Fact]
        public void Ignore_Words_Shorter_Than_3_Symbols()
        {
            var mockRepository = new Mock<IUserRepository>();
            var postList = new List<PostDTO>();
            postList.Add(new PostDTO { OwnerKey = "TestUser", PostId = 1, PostedAt = DateTime.Now, Text = "#UnitTest This is test string. #Un" });
            mockRepository.Setup(rep => rep.GetAllPosts(It.IsAny<string>())).Returns(postList);
            var controller = new TwitterController(mockRepository.Object);
            var result = controller.HashtagFrequency("Username", '#');
            Assert.Equal("[{\"Key\":\"#UnitTest\",\"cnt\":1}]", result);
        }
    }
}